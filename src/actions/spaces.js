import { handleInitialData } from '../actions/shared'
import { destroySpace } from '../helpers/api'

export function handleDestroySpace(id) {
  return (dispatch) => {
    destroySpace(id)
      .then(() => dispatch(handleInitialData()))
  }
}
