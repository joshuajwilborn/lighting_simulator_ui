import { getDevices, getDeviceTypes } from '../helpers/api'

export const RECEIVE_DATA = 'RECEIVE_DATA'

export function receiveData(devices, deviceTypes) {
  return {
    type: RECEIVE_DATA,
    devices,
    deviceTypes
  }
}

export function handleInitialData() {
  return (dispatch) => {
    Promise.all([
      getDevices(),
      getDeviceTypes()
    ]).then(( [devices, deviceTypes] ) => {
      dispatch(receiveData(devices, deviceTypes))
    })
  }
}
