export const ADD_DEVICE = 'ADD_DEVICE'
export const REMOVE_DEVICE = 'REMOVE_DEVICE'
export const UPDATE_DEVICE = 'UPDATE_DEVICE'

import * as api from '../helpers/api'

export function removeDevice(device) {
  return {
    type: REMOVE_DEVICE,
    device,
  }
}

export function addDevice(device) {
  return {
    type: ADD_DEVICE,
    device: device
  }
}

export function updateDevice(device) {
  return {
    type: UPDATE_DEVICE,
    device: device
  }
}

export function handleUpdateDevice(updated, old) {
  return (dispatch) => {
    dispatch(updateDevice(updated))
    api.updateDevice(updated)
      .catch(() => dispatch(updateDevice(old)))
  }
}

export function handleDestroyDevice(device) {
  return (dispatch) => {
    dispatch(removeDevice(device));
    api.destroyDevice(device.mac_address)
      .catch(() => dispatch(addDevice(device)))
  }
}

export function handleAddDevice() {
  return (dispatch) => {
    axios.createDevice()
      .then((device) => dispatch(addDevice(device)))
  }
}


