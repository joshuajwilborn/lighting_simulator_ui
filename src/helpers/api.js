import axios from 'axios';
const base = '/smartcast-sdk/v1'
const root = window.location.host.includes('localhost') ?
             'http://localhost:3000'
             :
             ''
export function getDevices() {
  return axios.get(root + base + '/devices')
    .then((response) => response.data);
}

export function getDeviceTypes() {
  return axios.get(root + base + '/device_types')
    .then((response) => response.data)
}

export function updateDevice(device) {
  return axios.put(root + base + '/devices/' + device.mac_address, stripDevice(device))
    .then(() => {
      return device;
    })
}

export function createSpace(opts) {
  
  return axios.post(root + base + '/spaces')
    .then(({data}) => {
      const { Space_id } = data
      const deviceOpts = {
        ...opts,
        space_id: Space_id,
        control_group_id: Space_id,
        network_id: 1
      }
      console.log('creating devices: ', deviceOpts);
      return createDevices(deviceOpts)
    })
}

export function destroySpace(id) {
  return axios.delete(root + base + '/spaces/' + id)
}

function createDevices(params) {
  return axios.post(root + base + '/devices', params)
}

export function destroyDevice(mac) {
  return axios.delete(root + base + '/devices/' + mac)
}
function stripDevice(device) {
  const { sensor_data, device_type } = device
  const { light_level , cct_value } = sensor_data
  const occ_group_id = device.occupancy_group_id
  return {
    cct_value,
    light_level,
    occ_group_id,
    device_type
  }
}
