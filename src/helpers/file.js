import * as api from '../helpers/api'
import axios from 'axios'
import { handleInitialData } from '../actions/shared.js'

const deviceCSVAttrs = [
  'lighting_network_id',
  'occupancy_group_name',
  'occupancy_group_id',
  'control_group_name',
  'control_group_id',
  'mac_address',
  'device_type'
]

function encodeCSV(string) {
  const fileType = 'data:text/csv;charset=utf-8,'
  return encodeURI(fileType + string)
}

function downloadEncodedLink(string) {
  const link = document.createElement('a')
  link.href = string
  link.download = 'cree_lighting_simulator.csv'
  link.style = 'visibility: none; display: none;'
  document.body.appendChild(link);
  link.click();
}

function buildCSVFromSelectedAttrs(attrs, objects) {
  return objects.map((object) => (
    attrs.reduce((list, attr) => {
      list.push(object[attr])
      return list
    }, []).join(',')
  )).join('\n')
}

export function downloadCSVFromDevices(devices) {
  const csv = buildCSVFromSelectedAttrs(deviceCSVAttrs, devices)
  const encoded = encodeCSV(csv)
  downloadEncodedLink(encoded)
}

export function spacesAndDevicesFromCSVFile({file, deviceTypes, dispatch}) {
  const reader = new FileReader()
  reader.onload = () =>  {
    const spaces = getSpacesFromCSVText(reader.result, deviceTypes)
    axios.all(spaces.map(api.createSpace)).then(() => {
      dispatch(handleInitialData())
    })
  }
  reader.readAsText(file)
}

function getSpacesFromCSVText(text, deviceTypes) {
  const customIntParse = (numString) => {
    const num = String(numString).match(/\d+/)
    if (num) return parseInt(num[0])
  }
  return text.trim().split('\n').reduce((spaces, row) => {
    const cells = row.trim().split(',').map((cell) => cell.trim())
    const id = customIntParse(cells[2])
    const found = spaces.find((space) => space.space_id === id)
    if (found) {
      found.quantity++
    } else {
      const space = {
        network_id: customIntParse(cells[0]),
        space_id: id,
        device_type: deviceTypes[cells[6]],
        quantity: 1
      }
      spaces.push(space)
    }
    return spaces
  }, [])
}
