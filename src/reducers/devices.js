import {
  ADD_DEVICE, REMOVE_DEVICE, UPDATE_DEVICE
} from '../actions/devices'
import { RECEIVE_DATA } from '../actions/shared'

export default function devices(state = [], action) {
  const { device, devices, type } = action
  switch(type) {
    case RECEIVE_DATA:
      return devices
    case ADD_DEVICE:
      const found = findDevice(device.mac_address)
      if (!found) {
        state.push(device)
      }
      return state
    case REMOVE_DEVICE:
      const index = state.indexOf(findDevice(device.mac_address))
      if (index >= 0) {
        return [
          ...state.slice(0, index),
          ...state.slice(index + 1)
        ]
      }
      return state
    case UPDATE_DEVICE: {
      const index = state.indexOf(findDevice(device.mac_address))
      if ( index >= 0 ) {
        return [
          ...state.slice(0, index),
          action.device,
          ...state.slice(index + 1)
        ]
      } else {
        return state.concat([ action.device ])
      }
    }
    default:
      return state
  }
  
  function findDevice(id) {
    const found = state.find((device) => device.mac_address === id)
    return found;
  }
}


