import { RECEIVE_DATA } from '../actions/shared.js'

export default function deviceTypes(state = null, action) {
  switch(action.type) {
    case RECEIVE_DATA:
      return action.deviceTypes
    default:
      return state
  }
}
