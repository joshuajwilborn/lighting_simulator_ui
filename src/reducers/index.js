import { combineReducers } from 'redux'

import devices from './devices'
import deviceTypes from './deviceTypes'

export default combineReducers({
  devices,
  deviceTypes
})

