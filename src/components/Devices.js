require('../../node_modules/font-awesome/css/font-awesome.min.css')
import React, { Component } from 'react'
import { all } from 'axios'
import { connect } from 'react-redux'
import EditDevice from './EditDevice'
import ReactTable from 'react-table'
import { handleUpdateDevice, handleDestroyDevice } from '../actions/devices'
import { handleDestroySpace } from '../actions/spaces'
import CreateSpace from './CreateSpace'
import { downloadCSVFromDevices, spacesAndDevicesFromCSVFile } from "../helpers/file"


export class Devices extends Component {
  constructor( props ) {
    super(props)
    
    this.state = {
      devices: null,
      showCreateSpace: false,
      editDevice: null
    }
    
    this.editDevice = this.editDevice.bind(this)
    this.saveDevice = this.saveDevice.bind(this)
    this.destroyDevice = this.destroyDevice.bind(this)
    this.showCreateSpace = this.showCreateSpace.bind(this)
    this.hideCreateSpace = this.hideCreateSpace.bind(this)
    this.destroySpace = this.destroySpace.bind(this)
    this.clickFileUpload = this.clickFileUpload.bind(this)
    this.handleUpload = this.handleUpload.bind(this)
    this.resetFileInput = this.resetFileInput.bind(this)
  }
  
  
  componentDidMount () {
    this.refs.fileUpload.onchange = this.handleUpload
  }
  
  handleUpload(event) {
    const file = event.target.files[0]
    const { dispatch, deviceTypes } = this.props
    spacesAndDevicesFromCSVFile({
      file,
      deviceTypes,
      dispatch
    })
    this.resetFileInput(event.target)
  }
  
  resetFileInput(el) {
    el.value = '';
    if(el.value) {
      el.type = "text";
      el.type = "file";
    }
  }
  
  clickFileUpload() {
    this.refs.fileUpload.click()
  }
  
  showCreateSpace() {
    this.setState(() => ({
      showCreateSpace: true
    }))
  }
  
  hideCreateSpace() {
    this.setState(() => ({
      showCreateSpace: false
    }))
  }
  
  editDevice(device) {
    this.setState(() => ({
      editDevice: device
    }))
  }
  
  destroyDevice(device) {
    const { dispatch } = this.props
    dispatch(handleDestroyDevice(device))
  }
  
  saveDevice(updated, old) {
    const { dispatch } = this.props
    dispatch(handleUpdateDevice(updated, old))
    this.setState(() => ({
      editDevice: null
    }))
  }
  
  destroySpace(id) {
    const { dispatch } = this.props
    dispatch(handleDestroySpace(id))
  }
  
  render() {
    return (
      <div>
        {this.state.editDevice &&
          <EditDevice
            device={this.state.editDevice}
            onHide={this.editDevice.bind(null, null)}
            onSave={this.saveDevice}
          />
        }
        {this.state.showCreateSpace &&
          <CreateSpace onFinish={this.hideCreateSpace}/>
        }
        {this.props.devices &&
          <ReactTable
            selectType='checkbox'
            className="-striped -highlight"
            headerClassName='py-20'
            data={this.props.devices}
            defaultPageSize={5}
            getTheadThProps={( ) => {
              return {
                className: 'py-20'
              }
            }}
            columns={[
              {
                Header: 'Network',
                accessor: 'lighting_network_id',
                sortable: true,
                style: {
                  textAlign: "center"
                }
              },
              {
                Header: 'Occ Group Name',
                accessor: 'occupancy_group_name',
                sortable: true,
                style: {
                  textAlign: "center"
                },
                Cell: (row) => {
                  return (
                    <div className='flex-evenly'>
                      <span>
                        { row.original.occupancy_group_name }
                      </span>
                      <span>
                        <button className="btn btn-link" onClick={this.destroySpace.bind(null, row.original.occupancy_group_id)}>
                          <i className="fa fa-trash text-danger"></i>
                        </button>
                      </span>
                    </div>
                  )
                }
              },
              {
                Header: 'Occ Group Id',
                accessor: 'occupancy_group_id',
                sortable: true,
                style: {
                  textAlign: "center"
                }
              },
              {
                Header: 'MAC',
                accessor: 'mac_address',
                sortable: true,
                style: {
                  textAlign: "center"
                }
              },
              {
                Header: 'Device Type',
                accessor: 'device_type',
                sortable: true,
                style: {
                  textAlign: "center"
                }
              },
              {
                Header: 'Light Level',
                accessor: 'sensor_data.light_level',
                sortable: true,
                style: {
                  textAlign: "center"
                }
              },
              {
                Header: 'CCT Level',
                accessor: 'sensor_data.cct_value',
                sortable: true,
                style: {
                  textAlign: "center"
                }
              },
              {
                Header: 'Actions',
                Cell: (row) => (
                  <div className='text-center' style={{display: 'flex', justifyContent: 'space-evenly'}}>
                    <button onClick={this.editDevice.bind(null, row.original)} className='btn-link'>
                      <i className='fa fa-edit text-primary'></i>
                    </button>
                    <button onClick={this.destroyDevice.bind(null, row.original)} className='btn-link'>
                      <i className="fa fa-trash text-danger"  ></i>
                    </button>
                  </div>
                )
              }
            ]}
          />
        }
        <div className='text-center mt-5 flex-evenly'>
          <button className="btn-primary btn" onClick={this.showCreateSpace.bind(null)}>
            <i className="fa fa-plus"></i>
            &nbsp;Space
          </button>
          <button className="btn-primary btn" onClick={downloadCSVFromDevices.bind(null, this.props.devices)}>
            <i className="fa fa-download"></i>
            &nbsp;Download CSV
          </button>
          <button className="btn-primary btn" onClick={this.clickFileUpload.bind(null)}>
            <input accept="*.csv" type='file' ref="fileUpload" style={{display: 'none'}}/>
            <i className="fa fa-download"></i>
            &nbsp;Upload CSV
          </button>
        </div>
      </div>
    );
  }
}

export default connect((state) => ({
  devices: state.devices,
  deviceTypes: state.deviceTypes
}))(Devices)
