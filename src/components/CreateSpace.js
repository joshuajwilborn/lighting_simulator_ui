import React, { Component } from 'react'
import { createSpace } from '../helpers/api'
import { Button, Modal} from 'react-bootstrap'
import { connect } from 'react-redux'
import { handleInitialData } from '../actions/shared'

class CreateSpace extends Component {
  constructor ( props ) {
    super(props);
    this.submit = this.submit.bind(this);
  }
  
  submit() {
    const {dispatch, onFinish} = this.props
    createSpace({
      device_type: Number(this.refs.type.value),
      quantity: Number(this.refs.quantity.value)
    }).then(() => {
      dispatch(handleInitialData());
      onFinish();
    })
  }
  
  render() {
    const { deviceTypes} = this.props
    const { onFinish } = this.props
    return (
      <Modal show={true} onHide={onFinish}>
        <Modal.Header closeButton>
          <Modal.Title>Create Space</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className="form-group">
              <label htmlFor="chooseSpace">Device Type </label>
              <select
                className='form-control-inline'
                ref='type'
              >
                {deviceTypes &&
                Object.keys(deviceTypes).map((type) => {
                  const id = deviceTypes[type]
                  return (
                    <option value={id} key={id}>{type} ({id})</option>
                  )
                })
                }
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputFile">Quantity</label>
              <input type="number" id="numDevices" min="1" max="100" step="1"
                     defaultValue={1}
                     ref='quantity'
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={onFinish}>Cancel</Button>
          <Button onClick={this.submit}>Submit</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default connect((state) => {
  return {
    deviceTypes: state.deviceTypes
  }
})(CreateSpace);
