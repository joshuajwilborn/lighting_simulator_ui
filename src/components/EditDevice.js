import React, { Component } from 'react'
import { Button, Modal} from 'react-bootstrap'
import SpacesSelector from './SpacesSelector'
import { connect } from 'react-redux'

class EditDevice extends Component {
  
  constructor(props) {
    super(props)
    this.device = JSON.parse(JSON.stringify(props.device))
    this.updateDevice = this.updateDevice.bind(this)
  }
  updateDevice(targetAttribute) {
    return ((event) => {
      let value = Number(event.target.value) || event.target.value
      eval(`this.device.${targetAttribute} = value`)
    }).bind(this)
  }
  
  render() {
    const { deviceTypes} = this.props
    const { onSave, onHide } = this.props
    const device = this.device
    return (
        <Modal show={true} onHide={this.props.onHide}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Device</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form>
              <div className="form-group">
                <label htmlFor="chooseSpace">Device Type </label>
                <select
                  className='form-control-inline'
                  defaultValue={device.device_type}
                  onChange={this.updateDevice("device_type")}
                >
                  {deviceTypes &&
                    Object.keys(deviceTypes).map((type) => {
                      const id = deviceTypes[type]
                      return (
                        <option value={type} key={id}>{type} ({id})</option>
                      )
                    })
                  }
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="chooseSpace">Occ Group</label>
                <SpacesSelector selectedSpace={device.occupancy_group_id} onChange={this.updateDevice('occupancy_group_id')}/>
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputFile">Light Level</label>
                <input type="number" id="cctLevel" min="0" max="100" step="1"
                       defaultValue={device.sensor_data.light_level}
                       onChange={this.updateDevice('sensor_data.light_level')}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputFile">CCT Level</label>
                <input type="number" id="cctLevel" min="3000" max="6000" step="100"
                       defaultValue={device.sensor_data.cct_value}
                       onChange={this.updateDevice('sensor_data.cct_level')}
                />
              </div>
            
            </form>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={onHide}>Cancel</Button>
            <Button onClick={onSave.bind(null, this.device, this.props.device)}>Save</Button>
          </Modal.Footer>
        </Modal>
    );
  }
}

export default connect((state) => {
  return {
    deviceTypes: state.deviceTypes
  }
})(EditDevice);
