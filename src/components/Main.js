require('../styles/App.css')
require('bootstrap/dist/css/bootstrap.css')
require('react-table/react-table.css')

import React       from 'react';
import { connect } from 'react-redux'
import {
  handleInitialData
} from '../actions/shared.js'

import ConnectedDevices from './Devices';

class AppComponent extends React.Component {

  componentDidMount () {
    const { dispatch } = this.props

    dispatch(handleInitialData())
  }

  render() {
    return (
      <div className="index">
        {/*<LoginManager/>*/}
        {/*<ContainerToggle/>*/}
        <ConnectedDevices/>
      </div>
    );
  }
}

export default connect((state) => ({
  devices: state.devices
}))(AppComponent);
