import { connect } from 'react-redux'
import React from 'react'

function SpaceSelector(props) {
  const { spaces } = props
  return (
    <select
      onChange={props.onChange}
      className="form-control-inline"
      id="chooseSpace"
      defaultValue={props.selectedSpace.id}>
      {spaces.map((space) => {
          return <option value={space.id} key={space.id}>{space.name}</option>}
      )}
    </select>
  )
}

export default connect((state) => {
  const spaces = state.devices.reduce((spaces, currentDevice) => {
    const id = currentDevice.occupancy_group_id
    const name = currentDevice.occupancy_group_name
    const found = spaces.find((space) => space.id === id)
    if (!found) spaces.push({ id, name })
    return spaces
  }, [])
  return {
    spaces,
  }
})(SpaceSelector)

