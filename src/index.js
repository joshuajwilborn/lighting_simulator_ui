import 'core-js/fn/object/assign'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/Main'
import reducer from './reducers'
import middleware from './middleware'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

const store = createStore(reducer, middleware)

// Render the main component into the dom
ReactDOM.render(
  <div>
    <header className='bg-primary'>
      <h2 className='text-center no-margin py-20'>Cree - Lighting Simulator Tool</h2>
    </header>
    <Provider store={store}>
      <App />
    </Provider>
  </div>

  , document.getElementById('app'));
