const express = require('express')
const app = express()
const fs = require('fs')
const base = '/Users/jwilborn/opensource/angular.js/node_modules/http2/example/'
const options = {
  key: fs.readFileSync(base + 'localhost.key'),
  cert: fs.readFileSync(base + 'localhost.crt'),
}
app.use(express.static('dist'))
require('spdy').createServer(options, app).listen(8000)
